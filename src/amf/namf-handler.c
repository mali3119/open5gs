/*
 * Copyright (C) 2019-2023 by Sukchan Lee <acetcom@gmail.com>
 *
 * This file is part of Open5GS.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <limits.h>

#include "namf-handler.h"
#include "nsmf-handler.h"

#include "nas-path.h"
#include "ngap-path.h"
#include "sbi-path.h"

#include "context.h"


// Helper functions

OpenAPI_list_t* create_session_list(amf_sess_t *session);
char* unsigned_char_str_to_char_str(unsigned char *source_char, unsigned int length);
size_t convert_hex(uint8_t *dest, size_t count, const char *src);

OpenAPI_list_t* create_session_list(amf_sess_t *session) {

    OpenAPI_list_t *amf_session_contexts = NULL;
    amf_session_contexts = OpenAPI_list_create();

    OpenAPI_pdu_session_context_t *pdu_session = ogs_calloc(1, sizeof(*pdu_session));
    pdu_session->pdu_session_id = session->psi;
    pdu_session->sm_context_ref = session->sm_context_ref;

    pdu_session->s_nssai = ogs_calloc(50, sizeof(*pdu_session->s_nssai));

    pdu_session->s_nssai->sd = ogs_s_nssai_sd_to_string(session->s_nssai.sd);
    pdu_session->s_nssai->sst = session->s_nssai.sst;
    pdu_session->dnn = session->dnn;
    pdu_session->access_type = OpenAPI_access_type_3GPP_ACCESS;


    OpenAPI_list_add(amf_session_contexts, pdu_session);

    return amf_session_contexts;

}

char* unsigned_char_str_to_char_str(unsigned char *source_str, unsigned int length) {
    
    char* target_str = malloc(sizeof(char));
    for(int x = 0; x < length; x++)
        sprintf(target_str+(x*2), "%02x", source_str[x]);

    target_str[length*2] = '\0';

    return target_str;

}

size_t convert_hex(uint8_t *dest, size_t count, const char *src) {
    char buf[3];
    size_t i;
    int value;
    for (i = 0; i < count && *src; i++) {
        buf[0] = *src++;
        buf[1] = '\0';
        if (*src) {
            buf[1] = *src++;
            buf[2] = '\0';
        }
        if (sscanf(buf, "%x", &value) != 1)
            break;
        dest[i] = value;
    }
    return i;
}


//==================================================================

ogs_sbi_request_t *amf_namf_build_ue_context(
        amf_ue_t *amf_ue, void *data)
{
    ogs_sbi_message_t message;
    ogs_sbi_request_t *request = NULL;

    amf_sess_t *session = (amf_sess_t *) data;

    OpenAPI_ue_context_create_data_t *ue_context = ogs_calloc(1, sizeof(*ue_context));


    ogs_assert(amf_ue);
    // ogs_assert(amf_ue->ur);

    // memset(ue_context, 0, sizeof(ue_context));
    memset(&message, 0, sizeof(message));

    message.h.method = (char *)OGS_SBI_HTTP_METHOD_PUT;
    
    message.h.service.name = (char *)OGS_SBI_SERVICE_NAME_NAMF_COMM;
    message.h.api.version = (char *)OGS_SBI_API_V1;
    message.h.resource.component[0] = (char *)OGS_SBI_RESOURCE_NAME_UE_CONTEXTS;
    message.h.resource.component[1] = amf_ue->supi;
    message.h.resource.component[2] = (char *)OGS_SBI_RESOURCE_NAME_CREATE;

    // Create UE Context message

    OpenAPI_global_ran_node_id_t *ranNodeId = NULL;
    OpenAPI_gnb_id_t *gNbId = NULL;
    OpenAPI_plmn_id_t *plmn_id = NULL;
    OpenAPI_tai_t *tai = NULL;
    uint8_t *handover = NULL;

    ranNodeId = ogs_calloc(1, sizeof(*ranNodeId));
    gNbId = ogs_calloc(1, sizeof(*gNbId));
    plmn_id = ogs_calloc(1, sizeof(*plmn_id));
    tai = ogs_calloc(1, sizeof(*tai));
    handover = ogs_calloc(1, sizeof(*handover));


    gNbId->g_nb_value = ogs_strdup("0009");
    gNbId->bit_length = 32;

    plmn_id->mcc = ogs_strdup("999");
    plmn_id->mnc = ogs_strdup("70");

    tai->plmn_id = plmn_id;
    tai->tac = ogs_strdup("7");

    ranNodeId->g_nb_id = (OpenAPI_gnb_id_t *) ogs_memdup(gNbId, sizeof(*gNbId));
    ranNodeId->plmn_id = plmn_id;    

    OpenAPI_ng_ran_target_id_t *targetId = ogs_calloc(1, sizeof(*targetId));
    targetId->ran_node_id = (OpenAPI_global_ran_node_id_t *) ogs_memdup(ranNodeId, sizeof(*ranNodeId));
    targetId->tai = tai;

    ue_context->target_id = targetId;
    // message.http.location = ogs_strdup("http://amf.5gc.mnc070.mcc999.3gppnetwork.org/");
    // message.h.uri = strcat(ogs_strdup("http://amf.5gc.mnc070.mcc999.3gppnetwork.org/namf-comm/v1/ue-contexts/"), amf_ue->supi);

    ue_context->ue_context = ogs_calloc(1, sizeof(*ue_context->ue_context));
    ue_context->ue_context->session_context_list = create_session_list(session);

    ue_context->source_to_target_data = ogs_calloc(50, sizeof(*ue_context->source_to_target_data));

    ue_context->source_to_target_data->is_ngap_message_type = true;
    ue_context->source_to_target_data->ngap_ie_type = OpenAPI_ngap_ie_type_PDU_RES_SETUP_REQ;
    ue_context->source_to_target_data->ngap_message_type = 5;

    // ogs_debug("Resource len: %d\n", session->transfer.pdu_session_resource_setup_request->len);
    // char *pdu_session_information = unsigned_char_str_to_char_str(session->transfer.pdu_session_resource_setup_request->data,
    //                                                               session->transfer.pdu_session_resource_setup_request->len);


    // convert_hex(ue_context->source_to_target_data->ngap_data, strlen(handover_request_pdu), handover_request_pdu);
    // s->transfer.handover_request->len = strlen(handover_request_pdu);
    ue_context->source_to_target_data->ngap_data = OpenAPI_ref_to_binary_data_create(ogs_strdup("ngap-sm"));

    ue_context->pdu_session_list = ogs_calloc(1, sizeof(*ue_context->pdu_session_list));
    // ue_context->supported_features = pdu_session_information;
    // ogs_asn_uint32_to_OCTET_STRING

    // message.ue_context->source_to_target_data = ogs_calloc(1, sizeof(*message.ue_context->source_to_target_data));


    message.ue_context = ue_context;


    // ogs_debug("OOPS: %s | Len: %d\n", session->payload_container->data, session->payload_container->len);

    message.part[0].pkbuf = session->payload_container;
    message.part[0].content_id = (char *)OGS_SBI_CONTENT_NGAP_SM_ID;
    message.part[0].content_type = (char *)OGS_SBI_CONTENT_NGAP_TYPE;
    message.num_of_part++;


    request = ogs_sbi_build_request(&message);
    ogs_assert(request);


    ogs_free(ranNodeId);
    ogs_free(gNbId);

    return request;
}

ogs_sbi_request_t *amf_namf_return_ue_context(
        amf_ue_t *amf_ue, void *data)
{
    ogs_sbi_message_t message;
    ogs_sbi_request_t *request = NULL;

    amf_sess_t *session = (amf_sess_t *) data;

    OpenAPI_ue_context_create_data_t *ue_context = ogs_calloc(1, sizeof(*ue_context));


    ogs_assert(amf_ue);
    // ogs_assert(amf_ue->ur);

    // memset(ue_context, 0, sizeof(ue_context));
    memset(&message, 0, sizeof(message));

    message.h.method = (char *)OGS_SBI_HTTP_METHOD_PUT;
    
    message.h.service.name = (char *)OGS_SBI_SERVICE_NAME_NAMF_COMM;
    message.h.api.version = (char *)OGS_SBI_API_V1;
    message.h.resource.component[0] = (char *)OGS_SBI_RESOURCE_NAME_UE_CONTEXTS;

    ogs_debug("SUPI: %s\n", amf_ue->supi);
    message.h.resource.component[1] = amf_ue->supi;
    message.h.resource.component[2] = (char *)OGS_SBI_RESOURCE_NAME_RETURN;

    // Create UE Context message

    OpenAPI_global_ran_node_id_t *ranNodeId = NULL;
    OpenAPI_gnb_id_t *gNbId = NULL;
    OpenAPI_plmn_id_t *plmn_id = NULL;
    OpenAPI_tai_t *tai = NULL;
    uint8_t *handover = NULL;

    ranNodeId = ogs_calloc(1, sizeof(*ranNodeId));
    gNbId = ogs_calloc(1, sizeof(*gNbId));
    plmn_id = ogs_calloc(1, sizeof(*plmn_id));
    tai = ogs_calloc(1, sizeof(*tai));
    handover = ogs_calloc(1, sizeof(*handover));


    gNbId->g_nb_value = ogs_strdup("0009");
    gNbId->bit_length = 32;

    plmn_id->mcc = ogs_strdup("999");
    plmn_id->mnc = ogs_strdup("70");

    tai->plmn_id = plmn_id;
    tai->tac = ogs_strdup("7");

    ranNodeId->g_nb_id = (OpenAPI_gnb_id_t *) ogs_memdup(gNbId, sizeof(*gNbId));
    ranNodeId->plmn_id = plmn_id;    

    OpenAPI_ng_ran_target_id_t *targetId = ogs_calloc(1, sizeof(*targetId));
    targetId->ran_node_id = (OpenAPI_global_ran_node_id_t *) ogs_memdup(ranNodeId, sizeof(*ranNodeId));
    targetId->tai = tai;

    ue_context->target_id = targetId;
    // message.http.location = ogs_strdup("http://amf.5gc.mnc070.mcc999.3gppnetwork.org/");
    // message.h.uri = strcat(ogs_strdup("http://amf.5gc.mnc070.mcc999.3gppnetwork.org/namf-comm/v1/ue-contexts/"), amf_ue->supi);

    ue_context->ue_context = ogs_calloc(1, sizeof(*ue_context->ue_context));
    ue_context->ue_context->session_context_list = create_session_list(session);

    ue_context->source_to_target_data = ogs_calloc(50, sizeof(*ue_context->source_to_target_data));

    ue_context->source_to_target_data->is_ngap_message_type = true;
    ue_context->source_to_target_data->ngap_ie_type = OpenAPI_ngap_ie_type_PDU_RES_SETUP_REQ;
    ue_context->source_to_target_data->ngap_message_type = 5;
    ue_context->source_to_target_data->ngap_data = OpenAPI_ref_to_binary_data_create(ogs_strdup("ngap-sm"));


    ue_context->pdu_session_list = ogs_calloc(1, sizeof(*ue_context->pdu_session_list));



    // ogs_asn_uint32_to_OCTET_STRING

    // message.ue_context->source_to_target_data = ogs_calloc(1, sizeof(*message.ue_context->source_to_target_data));


    message.ue_context = ue_context;


    // ogs_debug("OOPS: %s | Len: %d\n", session->payload_container->data, session->payload_container->len);

    message.part[0].pkbuf = session->payload_container;
    message.part[0].content_id = (char *)OGS_SBI_CONTENT_NGAP_SM_ID;
    message.part[0].content_type = (char *)OGS_SBI_CONTENT_NGAP_TYPE;
    message.num_of_part++;


    request = ogs_sbi_build_request(&message);
    ogs_assert(request);


    ogs_free(ranNodeId);
    ogs_free(gNbId);

    return request;
}

int amf_namf_comm_handle_ue_context(ogs_sbi_stream_t *stream, ogs_sbi_message_t *recvmsg){

    
    // =====================================================================================
    NGAP_TargetID_t *TargetID = ogs_calloc(1, sizeof(NGAP_TargetID_t));

    amf_sess_t *sess = NULL;
    amf_ue_t *amf_ue = NULL;
    amf_ue_t *target_amf_ue = NULL;
    ran_ue_t *target_ue =  NULL;
    ran_ue_t *source_ue =  NULL;



    // ogs_ngap_message_t handover_required;
    // NGAP_InitiatingMessage_t *initiatingMessage = NULL;
    // NGAP_HandoverRequired_t *HandoverRequired = NULL;
    // NGAP_HandoverType_t *HandoverType = NULL;
    // NGAP_HandoverRequiredIEs_t *ie = NULL;

    

    sess = alloc_session(sess);
    sess->amf_ue = alloc_ue(amf_ue);

    amf_nsmf_pdusession_sm_context_param_t param;

    OpenAPI_ng_ran_target_id_t *ranNodeId = recvmsg->ue_context->target_id;

    OpenAPI_lnode_t *node = NULL;
    OpenAPI_pdu_session_context_t *pdu_session = NULL;


    OpenAPI_list_for_each(recvmsg->ue_context->ue_context->session_context_list, node) {
        pdu_session = node->data;

        sess->psi = pdu_session->pdu_session_id;
        sess->sm_context_ref = malloc(sizeof(char));

        ogs_debug("SM_CONTEXT: %s\n", pdu_session->sm_context_ref);
        strcpy(sess->sm_context_ref, pdu_session->sm_context_ref);
        

    }



    // ogs_ngap_decode(&handover_required, recvmsg->part[0].pkbuf);


    OCTET_STRING_t *transfer = ogs_calloc(1, sizeof(*transfer)); 
    OCTET_STRING_fromString(transfer, "\n");

    TargetID->present = NGAP_TargetID_PR_targetRANNodeID;

    TargetID->choice.targetRANNodeID = ogs_calloc(1, sizeof(*TargetID->choice.targetRANNodeID));

    TargetID->choice.targetRANNodeID->globalRANNodeID.present = NGAP_GlobalRANNodeID_PR_globalGNB_ID;
    TargetID->choice.targetRANNodeID->globalRANNodeID.choice.globalGNB_ID = ogs_calloc(1, sizeof(*TargetID->choice.targetRANNodeID->globalRANNodeID.choice.globalGNB_ID));


    uint32_t gnb_id =  atoi(ranNodeId->ran_node_id->g_nb_id->g_nb_value);


    ogs_ngap_uint32_to_GNB_ID(gnb_id, ranNodeId->ran_node_id->g_nb_id->bit_length, 
                              &TargetID->choice.targetRANNodeID->globalRANNodeID.choice.globalGNB_ID->gNB_ID);




    ogs_plmn_id_t plmn;  
    char* mcc = ranNodeId->ran_node_id->plmn_id->mcc;
    char* mnc = ranNodeId->ran_node_id->plmn_id->mnc;

    ogs_plmn_id_build(&plmn, atoi(mcc), atoi(mnc), strlen(mnc));

    ogs_plmn_id_t vplmn;  
    char* vmcc = ogs_strdup("001");
    char* vmnc = ogs_strdup("01");

    ogs_plmn_id_build(&vplmn, atoi(vmcc), atoi(vmnc), strlen(vmnc));

    sess->amf_ue->last_visited_plmn_id = vplmn;
    sess->amf_ue->home_plmn_id  = plmn;

    ogs_asn_buffer_to_OCTET_STRING(&plmn, sizeof(ogs_plmn_id_t),
            &TargetID->choice.targetRANNodeID->globalRANNodeID.choice.globalGNB_ID->pLMNIdentity);
    
    ogs_5gs_tai_t tai;
    tai.plmn_id = plmn;
    tai.tac.v = 1;

    ogs_asn_buffer_to_OCTET_STRING(&plmn, sizeof(ogs_plmn_id_t),
            &TargetID->choice.targetRANNodeID->selectedTAI.pLMNIdentity);

    ogs_asn_buffer_to_OCTET_STRING(&tai, sizeof(ogs_5gs_tai_t),
            &TargetID->choice.targetRANNodeID->selectedTAI.tAC);

    //==========================================================================================

    // memcpy(TargetID->choice.targetRANNodeID->globalRANNodeID.choice.globalGNB_ID->pLMNIdentity.buf,
    //        &plmn, sizeof(plmn));


    // TargetID->choice.targetRANNodeID->globalRANNodeID.choice.globalGNB_ID->pLMNIdentity.size = ;


    // ogs_asn_buffer_to_OCTET_STRING(
    //         ranNodeId->ran_node_id->plmn_id., OGS_PLMN_ID_LEN, );
    // TargetID->choice.targetRANNodeID->globalRANNodeID.choice.globalGNB_ID->pLMNIdentity.
    // TargetID->choice.targeteNB_ID = ogs_s1ap_EN


    //===========================================================================================
    // Creation of Target RAN UE and target AMF UE

    sess->amf_ue->ran_ue = alloc_ran_ue(sess->amf_ue->ran_ue);

    sess->amf_ue->ran_ue->target_ue = alloc_ran_ue(sess->amf_ue->ran_ue->target_ue);
    sess->amf_ue->ran_ue->target_ue->source_ue = alloc_ran_ue(sess->amf_ue->ran_ue->target_ue->source_ue);
    sess->amf_ue->ran_ue->target_ue->amf_ue = alloc_ue(sess->amf_ue->ran_ue->target_ue->amf_ue);

    target_ue = sess->amf_ue->ran_ue->target_ue;
    target_ue->amf_ue_ngap_id = atoi(sess->sm_context_ref) + 1;
    target_ue->ran_ue_ngap_id = -1;
    target_ue->gnb = amf_gnb_find_by_gnb_id(gnb_id);
    target_ue->gnb_ostream_id = OGS_NEXT_ID(target_ue->gnb->ostream_id, 1, target_ue->gnb->max_num_of_ostreams-1);

    source_ue = sess->amf_ue->ran_ue->target_ue->source_ue;
    source_ue->amf_ue_ngap_id = atoi(sess->sm_context_ref);
    source_ue->amf_ue_ngap_id = 1;


    // initiatingMessage = handover_required.choice.initiatingMessage;
    // HandoverRequired = &initiatingMessage->value.choice.HandoverRequired;

    // for (int i = 0; i < HandoverRequired->protocolIEs.list.count; i++) {
    //     ie = HandoverRequired->protocolIEs.list.array[i];
    //     switch (ie->id) {
    //     case NGAP_ProtocolIE_ID_id_HandoverType:
    //         HandoverType = &ie->value.choice.HandoverType;
    //         break;
    //     default:
    //         break;
    //     }
    // }


    target_amf_ue = sess->amf_ue->ran_ue->target_ue->amf_ue;
    target_amf_ue->handover.type = NGAP_HandoverType_intra5gs;

    target_amf_ue->last_visited_plmn_id = vplmn;
    target_amf_ue->home_plmn_id = plmn;


    target_amf_ue->handover.cause = 16;
    target_amf_ue->handover.group = 1;

    target_amf_ue->ue_ambr.downlink =  1000000000;
    target_amf_ue->ue_ambr.uplink =  1000000000;

    target_amf_ue->ue_security_capability.nr_ea2  = 1;
    target_amf_ue->ue_security_capability.nr_ia2  = 1;

    target_amf_ue->supi = malloc(sizeof(char));
    strcpy(target_amf_ue->supi, recvmsg->h.resource.component[1]);

    char *nh = ogs_strdup("21980964227a1fc670430c0add63e1dfe2330e5a9e4a0dc237d22cf7135734a7");
    target_amf_ue->nhcc = 2;

    for (int i = 0; i < 32; i+= 1){

        char nh_part[2];
        nh_part[0] = nh[i];
        nh_part[1] = nh[i+1];

        target_amf_ue->nh[i] = atoi(nh_part);
    }
    

    amf_sess_t *s = ogs_calloc(1, sizeof(amf_sess_t));
    s->psi = 1;
    s->s_nssai.sst = 1;
    s->sm_context_ref = sess->sm_context_ref;
    s->amf_ue = sess->amf_ue;
    ogs_debug("From resource: %s | SUPI: %s", recvmsg->h.resource.component[1], s->amf_ue->supi);
    s->dnn = ogs_strdup("srsapn");

    char *handover_request_pdu = ogs_strdup("0000050082000a0c3b9aca00303b9aca00008b000a01f07f00000700003c3b007f00010000860001000088000700010000091c00");

    s->transfer.handover_request = ogs_pkbuf_alloc(NULL, OGS_MAX_SDU_LEN);

    convert_hex(s->transfer.handover_request->data, strlen(handover_request_pdu), handover_request_pdu);
    s->transfer.handover_request->len = strlen(handover_request_pdu);

    ogs_list_add(&target_amf_ue->sess_list, (void *)s);

    // char *container = ogs_strdup("500300001100000100010000f11000000900000000000000f110000009000000000a");
    // target_amf_ue->handover.container.buf = ogs_calloc(1, sizeof(uint8_t));

    // for (int i = 0; i < 34; i+= 1){

    //     char container_part[2];
    //     container_part[0] = container[i * 2];
    //     container_part[1] = container[(i * 2)+1];

    //     target_amf_ue->handover.container.buf[i] = atoi(container_part);
    // }

    // target_amf_ue->handover.container.size = 34;

    char *_container = ogs_strdup("500300001100000100010000f11000000900000000000000f110000009000000000a");
    
    target_amf_ue->handover.container.buf = CALLOC(strlen(_container), sizeof(uint8_t));
    target_amf_ue->handover.container.size = strlen(_container);

    convert_hex(target_amf_ue->handover.container.buf, strlen(_container), _container);
    // uint8_t tmp[OGS_HUGE_LEN];

    // ogs_hex_from_string(_container, tmp, sizeof(tmp));
    // target_amf_ue->handover.container.buf = CALLOC(35, sizeof(uint8_t));
    // memcpy(target_amf_ue->handover.container.buf,
    //        tmp, 35);
    // target_amf_ue->handover.container.size = 35;

    // target_amf_ue->handover.container.buf = (uint8_t *)tmp;


    target_amf_ue->masked_imeisv_len = 16;
    target_amf_ue->masked_imeisv[0] = 0x11;
    target_amf_ue->masked_imeisv[1] = 0x10;
    target_amf_ue->masked_imeisv[2] = 0x00;
    target_amf_ue->masked_imeisv[3] = 0x00;
    target_amf_ue->masked_imeisv[4] = 0x00;
    target_amf_ue->masked_imeisv[5] = 0xff;
    target_amf_ue->masked_imeisv[6] = 0xff;
    target_amf_ue->masked_imeisv[7] = 0x00;

    target_amf_ue->allowed_nssai.num_of_s_nssai = 1;
    target_amf_ue->allowed_nssai.s_nssai->sst = 01;

    target_amf_ue->guami = ogs_calloc(1, sizeof(ogs_guami_t));
    
    
    target_amf_ue->guami->amf_id.pointer = 0;
    target_amf_ue->guami->amf_id.set1 = 40;
    target_amf_ue->guami->amf_id.region = 02;

    memcpy(&target_amf_ue->guami->plmn_id, &plmn, sizeof(ogs_plmn_id_t));





    //====================================================================
    memset(&param, 0, sizeof(param));
    param.n2smbuf = ogs_pkbuf_alloc(NULL, OGS_MAX_SDU_LEN);
    // ogs_assert(param.n2smbuf);
    param.n2SmInfoType = OpenAPI_n2_sm_info_type_HANDOVER_REQUIRED;
    ogs_pkbuf_put_data(param.n2smbuf, transfer->buf, transfer->size);

    param.hoState = OpenAPI_ho_state_PREPARING;
    param.TargetID = TargetID;

    amf_sess_sbi_discover_and_send(
                OGS_SBI_SERVICE_TYPE_NSMF_PDUSESSION, NULL,
                amf_nsmf_pdusession_build_update_sm_context,
                sess, AMF_UPDATE_SM_CONTEXT_HANDOVER_REQUIRED, &param);
    //====================================================================

    // amf_gnb_t *target_gnb = NULL;
    // ran_ue_t *target_ue = NULL;



    // if (recvmsg->ue_context == NULL){
    //     ogs_debug(":(((((((())))))))");
    // }

    // OpenAPI_ue_context_create_data_t *ue_context = recvmsg->ue_context;

    // OpenAPI_global_ran_node_id_t *target_id = ue_context->target_id->ran_node_id;

    // char *gnb_id = target_id->g_nb_id->g_nb_value;
    // uint32_t gnb_int = atoi(gnb_id);

    // target_gnb = amf_gnb_find_by_gnb_id(gnb_int);

    // if (target_gnb == NULL){
    //     ogs_debug("OOPS: %d\n", gnb_int);
    // } else {
    //     ogs_debug("GNBID: %d\n", target_gnb->gnb_id);
    // }

    // target_ue = ran_ue_add(target_gnb, INVALID_UE_NGAP_ID);

    // if (target_ue == NULL) {
    //     ogs_debug("Target UE null\n");
    // }

    // amf_ue = amf_ue_find_by_supi(recvmsg->h.resource.component[1]);
    // if (!amf_ue) {
    //     int status = OGS_SBI_HTTP_STATUS_NOT_FOUND;
    //     ogs_error("Cannot find SUPI [%s] [%d]", recvmsg->h.resource.component[1], status);
    // }

    // ogs_debug("At last\n");

    return OGS_OK;
}

int amf_namf_comm_handle_return_ue_context(ogs_sbi_stream_t *stream, ogs_sbi_message_t *recvmsg){

    
    // =====================================================================================
    // NGAP_TargetID_t *TargetID = ogs_calloc(1, sizeof(NGAP_TargetID_t));


    amf_sess_t *sess = NULL;
    amf_ue_t *amf_ue = NULL;
    amf_ue_t *source_amf_ue = NULL;

    // amf_ue_t *target_amf_ue = NULL;
    // ran_ue_t *target_ue =  NULL;
    // ran_ue_t *source_ue =  NULL;

    amf_ue = amf_ue_find_by_supi(recvmsg->h.resource.component[1]);
    if (!amf_ue) {
        ogs_error("No UE context [%s]", recvmsg->h.resource.component[1]);
        return OGS_ERROR;
    }

    sess = amf_sess_find_by_psi(amf_ue, 1);
    if (!sess) {
        ogs_error("[%s] No PDU Session Context [%d]",
                amf_ue->supi, 1);
        return OGS_ERROR;
    }

    sess->amf_ue = amf_ue;

    amf_nsmf_pdusession_sm_context_param_t param;

    OpenAPI_ng_ran_target_id_t *ranNodeId = recvmsg->ue_context->target_id;

    OpenAPI_lnode_t *node = NULL;
    OpenAPI_pdu_session_context_t *pdu_session = NULL;


    OpenAPI_list_for_each(recvmsg->ue_context->ue_context->session_context_list, node) {
        pdu_session = node->data;

        // sess->psi = pdu_session->pdu_session_id;
        sess->sm_context_ref = malloc(sizeof(char));

        strcpy(sess->sm_context_ref, pdu_session->sm_context_ref);
        
    }

    // char* n2msg = unsigned_char_str_to_char_str(recvmsg->part[0].pkbuf->data, recvmsg->part[0].pkbuf->len);

    // OCTET_STRING_t *transfer = ogs_calloc(1, sizeof(*transfer)); 
    // OCTET_STRING_fromString(transfer, n2msg);

    // TargetID->present = NGAP_TargetID_PR_targetRANNodeID;

    // TargetID->choice.targetRANNodeID = ogs_calloc(1, sizeof(*TargetID->choice.targetRANNodeID));

    // TargetID->choice.targetRANNodeID->globalRANNodeID.present = NGAP_GlobalRANNodeID_PR_globalGNB_ID;
    // TargetID->choice.targetRANNodeID->globalRANNodeID.choice.globalGNB_ID = ogs_calloc(1, sizeof(*TargetID->choice.targetRANNodeID->globalRANNodeID.choice.globalGNB_ID));


    // uint32_t gnb_id =  atoi(ranNodeId->ran_node_id->g_nb_id->g_nb_value);


    // ogs_ngap_uint32_to_GNB_ID(gnb_id, ranNodeId->ran_node_id->g_nb_id->bit_length, 
    //                           &TargetID->choice.targetRANNodeID->globalRANNodeID.choice.globalGNB_ID->gNB_ID);




    ogs_plmn_id_t plmn;  
    char* mcc = ranNodeId->ran_node_id->plmn_id->mcc;
    char* mnc = ranNodeId->ran_node_id->plmn_id->mnc;

    ogs_plmn_id_build(&plmn, atoi(mcc), atoi(mnc), strlen(mnc));

    source_amf_ue = sess->amf_ue->ran_ue->amf_ue;

    // ogs_plmn_id_t vplmn;  
    // char* vmcc = ogs_strdup("001");
    // char* vmnc = ogs_strdup("01");

    // ogs_plmn_id_build(&vplmn, atoi(vmcc), atoi(vmnc), strlen(vmnc));

    sess->amf_ue->last_visited_plmn_id = sess->amf_ue->home_plmn_id = plmn;

    char *_container = ogs_strdup("0003000011");
    
    source_amf_ue->handover.container.buf = CALLOC(strlen(_container), sizeof(uint8_t));
    source_amf_ue->handover.container.size = strlen(_container);

    convert_hex(source_amf_ue->handover.container.buf, strlen(_container) / 2, _container);

    // ogs_asn_buffer_to_OCTET_STRING(&plmn, sizeof(ogs_plmn_id_t),
    //         &TargetID->choice.targetRANNodeID->globalRANNodeID.choice.globalGNB_ID->pLMNIdentity);
    
    // ogs_5gs_tai_t tai;
    // tai.plmn_id = plmn;
    // tai.tac.v = 1;

    // ogs_asn_buffer_to_OCTET_STRING(&plmn, sizeof(ogs_plmn_id_t),
    //         &TargetID->choice.targetRANNodeID->selectedTAI.pLMNIdentity);

    // ogs_asn_buffer_to_OCTET_STRING(&tai, sizeof(ogs_5gs_tai_t),
    //         &TargetID->choice.targetRANNodeID->selectedTAI.tAC);


    // //===========================================================================================
    // // Creation of Target RAN UE and target AMF UE

    // sess->amf_ue->ran_ue = alloc_ran_ue(sess->amf_ue->ran_ue);

    // sess->amf_ue->ran_ue->target_ue = alloc_ran_ue(sess->amf_ue->ran_ue->target_ue);
    // sess->amf_ue->ran_ue->target_ue->source_ue = alloc_ran_ue(sess->amf_ue->ran_ue->target_ue->source_ue);
    // sess->amf_ue->ran_ue->target_ue->amf_ue = alloc_ue(sess->amf_ue->ran_ue->target_ue->amf_ue);

    // target_ue = sess->amf_ue->ran_ue->target_ue;
    // target_ue->amf_ue_ngap_id = atoi(sess->sm_context_ref) + 1;
    // target_ue->ran_ue_ngap_id = -1;
    // target_ue->gnb = amf_gnb_find_by_gnb_id(gnb_id);
    // target_ue->gnb_ostream_id = OGS_NEXT_ID(target_ue->gnb->ostream_id, 1, target_ue->gnb->max_num_of_ostreams-1);

    // source_ue = sess->amf_ue->ran_ue->target_ue->source_ue;
    // source_ue->amf_ue_ngap_id = atoi(sess->sm_context_ref);
    // source_ue->amf_ue_ngap_id = 1;


    // target_amf_ue = sess->amf_ue->ran_ue->target_ue->amf_ue;
    // target_amf_ue->handover.type = NGAP_HandoverType_intra5gs;

    // target_amf_ue->last_visited_plmn_id = vplmn;
    // target_amf_ue->home_plmn_id = plmn;


    // target_amf_ue->handover.cause = 16;
    // target_amf_ue->handover.group = 1;

    // target_amf_ue->ue_ambr.downlink =  1000000000;
    // target_amf_ue->ue_ambr.uplink =  1000000000;

    // target_amf_ue->ue_security_capability.nr_ea2  = 1;
    // target_amf_ue->ue_security_capability.nr_ia2  = 1;

    // target_amf_ue->supi = malloc(sizeof(char));
    // strcpy(target_amf_ue->supi, recvmsg->h.resource.component[1]);

    // char *nh = ogs_strdup("21980964227a1fc670430c0add63e1dfe2330e5a9e4a0dc237d22cf7135734a7");
    // target_amf_ue->nhcc = 2;

    // for (int i = 0; i < 32; i+= 1){

    //     char nh_part[2];
    //     nh_part[0] = nh[i];
    //     nh_part[1] = nh[i+1];

    //     target_amf_ue->nh[i] = atoi(nh_part);
    // }
    

    // amf_sess_t *s = ogs_calloc(1, sizeof(amf_sess_t));
    // s->psi = 1;
    // s->s_nssai.sst = 1;
    // s->sm_context_ref = sess->sm_context_ref;
    // s->amf_ue = sess->amf_ue;
    // ogs_debug("From resource: %s | SUPI: %s", recvmsg->h.resource.component[1], s->amf_ue->supi);
    // s->dnn = ogs_strdup("srsapn");

    // char *handover_request_pdu = ogs_strdup("0000050082000a0c3b9aca00303b9aca00008b000a01f07f00000700003c3b007f00010000860001000088000700010000091c00");


    // s->transfer.handover_request = ogs_pkbuf_alloc(NULL, OGS_MAX_SDU_LEN);

    // convert_hex(s->transfer.handover_request->data, strlen(handover_request_pdu), handover_request_pdu);
    // s->transfer.handover_request->len = strlen(handover_request_pdu);

    // ogs_list_add(&target_amf_ue->sess_list, (void *)s);

    // char *_container = ogs_strdup("500300001100000100010000f11000000900000000000000f110000009000000000a");
    
    // target_amf_ue->handover.container.buf = CALLOC(strlen(_container), sizeof(uint8_t));
    // target_amf_ue->handover.container.size = strlen(_container);

    // convert_hex(target_amf_ue->handover.container.buf, strlen(_container), _container);

    // target_amf_ue->masked_imeisv_len = 16;
    // target_amf_ue->masked_imeisv[0] = 0x11;
    // target_amf_ue->masked_imeisv[1] = 0x10;
    // target_amf_ue->masked_imeisv[2] = 0x00;
    // target_amf_ue->masked_imeisv[3] = 0x00;
    // target_amf_ue->masked_imeisv[4] = 0x00;
    // target_amf_ue->masked_imeisv[5] = 0xff;
    // target_amf_ue->masked_imeisv[6] = 0xff;
    // target_amf_ue->masked_imeisv[7] = 0x00;

    // target_amf_ue->allowed_nssai.num_of_s_nssai = 1;
    // target_amf_ue->allowed_nssai.s_nssai->sst = 01;

    // target_amf_ue->guami = ogs_calloc(1, sizeof(ogs_guami_t));
    
    
    // target_amf_ue->guami->amf_id.pointer = 0;
    // target_amf_ue->guami->amf_id.set1 = 40;
    // target_amf_ue->guami->amf_id.region = 02;

    // memcpy(&target_amf_ue->guami->plmn_id, &plmn, sizeof(ogs_plmn_id_t));

    //====================================================================
    memset(&param, 0, sizeof(param));
    param.n2smbuf = ogs_pkbuf_alloc(NULL, OGS_MAX_SDU_LEN);
    // ogs_assert(param.n2smbuf);
    param.n2SmInfoType = OpenAPI_n2_sm_info_type_HANDOVER_REQ_ACK;
    ogs_pkbuf_put_data(param.n2smbuf, recvmsg->part[0].pkbuf->data, recvmsg->part[0].pkbuf->len);

    param.hoState = OpenAPI_ho_state_PREPARED;

    amf_sess_sbi_discover_and_send(
                OGS_SBI_SERVICE_TYPE_NSMF_PDUSESSION, NULL,
                amf_nsmf_pdusession_build_update_sm_context,
                sess, AMF_UPDATE_SM_CONTEXT_HANDOVER_REQ_ACK, &param);
    //====================================================================



    return OGS_OK;
}


int amf_namf_comm_handle_n1_n2_message_transfer(
        ogs_sbi_stream_t *stream, ogs_sbi_message_t *recvmsg)
{
    int status, r;

    amf_ue_t *amf_ue = NULL;
    ran_ue_t *ran_ue = NULL;
    amf_sess_t *sess = NULL;

    ogs_pkbuf_t *n1buf = NULL;
    ogs_pkbuf_t *n2buf = NULL;

    ogs_pkbuf_t *gmmbuf = NULL;
    ogs_pkbuf_t *ngapbuf = NULL;

    char *supi = NULL;
    uint8_t pdu_session_id = OGS_NAS_PDU_SESSION_IDENTITY_UNASSIGNED;

    ogs_sbi_message_t sendmsg;
    ogs_sbi_response_t *response = NULL;

    OpenAPI_n1_n2_message_transfer_req_data_t *N1N2MessageTransferReqData;
    OpenAPI_n1_n2_message_transfer_rsp_data_t N1N2MessageTransferRspData;
    OpenAPI_n1_message_container_t *n1MessageContainer = NULL;
    OpenAPI_ref_to_binary_data_t *n1MessageContent = NULL;
    OpenAPI_n2_info_container_t *n2InfoContainer = NULL;
    OpenAPI_n2_sm_information_t *smInfo = NULL;
    OpenAPI_n2_info_content_t *n2InfoContent = NULL;
    OpenAPI_ref_to_binary_data_t *ngapData = NULL;

    OpenAPI_ngap_ie_type_e ngapIeType = OpenAPI_ngap_ie_type_NULL;

    ogs_assert(stream);
    ogs_assert(recvmsg);

    N1N2MessageTransferReqData = recvmsg->N1N2MessageTransferReqData;
    if (!N1N2MessageTransferReqData) {
        ogs_error("No N1N2MessageTransferReqData");
        return OGS_ERROR;
    }

    if (N1N2MessageTransferReqData->is_pdu_session_id == false) {
        ogs_error("No PDU Session Identity");
        return OGS_ERROR;
    }
    pdu_session_id = N1N2MessageTransferReqData->pdu_session_id;

    supi = recvmsg->h.resource.component[1];
    if (!supi) {
        ogs_error("No SUPI");
        return OGS_ERROR;
    }

    amf_ue = amf_ue_find_by_supi(supi);
    if (!amf_ue) {
        ogs_error("No UE context [%s]", supi);
        return OGS_ERROR;
    }

    sess = amf_sess_find_by_psi(amf_ue, pdu_session_id);
    if (!sess) {
        ogs_error("[%s] No PDU Session Context [%d]",
                amf_ue->supi, pdu_session_id);
        return OGS_ERROR;
    }

    n1MessageContainer = N1N2MessageTransferReqData->n1_message_container;
    if (n1MessageContainer) {
        n1MessageContent = n1MessageContainer->n1_message_content;
        if (!n1MessageContent || !n1MessageContent->content_id) {
            ogs_error("No n1MessageContent");
            return OGS_ERROR;
        }

        n1buf = ogs_sbi_find_part_by_content_id(
                recvmsg, n1MessageContent->content_id);
        if (!n1buf) {
            ogs_error("[%s] No N1 SM Content", amf_ue->supi);
            return OGS_ERROR;
        }

        /*
         * NOTE : The pkbuf created in the SBI message will be removed
         *        from ogs_sbi_message_free(), so it must be copied.
         */
        n1buf = ogs_pkbuf_copy(n1buf);
        ogs_assert(n1buf);
    }

    n2InfoContainer = N1N2MessageTransferReqData->n2_info_container;
    if (n2InfoContainer) {
        smInfo = n2InfoContainer->sm_info;
        if (!smInfo) {
            ogs_error("No smInfo");
            return OGS_ERROR;
        }

        n2InfoContent = smInfo->n2_info_content;
        if (!n2InfoContent) {
            ogs_error("No n2InfoContent");
            return OGS_ERROR;
        }

        ngapIeType = n2InfoContent->ngap_ie_type;

        ngapData = n2InfoContent->ngap_data;
        if (!ngapData || !ngapData->content_id) {
            ogs_error("No ngapData");
            return OGS_ERROR;
        }
        n2buf = ogs_sbi_find_part_by_content_id(
                recvmsg, ngapData->content_id);
        if (!n2buf) {
            ogs_error("[%s] No N2 SM Content", amf_ue->supi);
            return OGS_ERROR;
        }

        /*
         * NOTE : The pkbuf created in the SBI message will be removed
         *        from ogs_sbi_message_free(), so it must be copied.
         */
        n2buf = ogs_pkbuf_copy(n2buf);
        ogs_assert(n2buf);
    }

    memset(&sendmsg, 0, sizeof(sendmsg));

    status = OGS_SBI_HTTP_STATUS_OK;

    memset(&N1N2MessageTransferRspData, 0, sizeof(N1N2MessageTransferRspData));
    N1N2MessageTransferRspData.cause =
        OpenAPI_n1_n2_message_transfer_cause_N1_N2_TRANSFER_INITIATED;

    sendmsg.N1N2MessageTransferRspData = &N1N2MessageTransferRspData;

    switch (ngapIeType) {
    case OpenAPI_ngap_ie_type_PDU_RES_SETUP_REQ:
        if (!n2buf) {
            ogs_error("[%s] No N2 SM Content", amf_ue->supi);
            return OGS_ERROR;
        }

        if (n1buf) {
            gmmbuf = gmm_build_dl_nas_transport(sess,
                    OGS_NAS_PAYLOAD_CONTAINER_N1_SM_INFORMATION, n1buf, 0, 0);
            ogs_assert(gmmbuf);
        }

        if (gmmbuf) {
            /***********************************
             * 4.3.2 PDU Session Establishment *
             ***********************************/

            ran_ue = ran_ue_cycle(amf_ue->ran_ue);
            if (ran_ue) {
                if (sess->pdu_session_establishment_accept) {
                    ogs_pkbuf_free(sess->pdu_session_establishment_accept);
                    sess->pdu_session_establishment_accept = NULL;
                }

                if (ran_ue->initial_context_setup_request_sent == true) {
                    ngapbuf =
                        ngap_sess_build_pdu_session_resource_setup_request(
                            sess, gmmbuf, n2buf);
                    ogs_assert(ngapbuf);
                } else {
                    ngapbuf = ngap_sess_build_initial_context_setup_request(
                            sess, gmmbuf, n2buf);
                    ogs_assert(ngapbuf);

                    ran_ue->initial_context_setup_request_sent = true;
                }

                if (SESSION_CONTEXT_IN_SMF(sess)) {
                /*
                 * [1-CLIENT] /nsmf-pdusession/v1/sm-contexts
                 * [2-SERVER] /namf-comm/v1/ue-contexts/{supi}/n1-n2-messages
                 *
                 * If [2-SERVER] arrives after [1-CLIENT],
                 * sm-context-ref is created in [1-CLIENT].
                 * So, the PDU session establishment accpet can be transmitted.
                 */
                    r = ngap_send_to_ran_ue(ran_ue, ngapbuf);
                    ogs_expect(r == OGS_OK);
                    ogs_assert(r != OGS_ERROR);
                } else {
                    sess->pdu_session_establishment_accept = ngapbuf;
                }
            } else {
                ogs_warn("[%s] RAN-NG Context has already been removed",
                            amf_ue->supi);
            }

        } else {
            /*********************************************
             * 4.2.3.3 Network Triggered Service Request *
             *********************************************/

            if (CM_IDLE(amf_ue)) {
                bool rc;
                ogs_sbi_server_t *server = NULL;
                ogs_sbi_header_t header;
                ogs_sbi_client_t *client = NULL;
                OpenAPI_uri_scheme_e scheme = OpenAPI_uri_scheme_NULL;
                char *fqdn = NULL;
                uint16_t fqdn_port = 0;
                ogs_sockaddr_t *addr = NULL, *addr6 = NULL;

                if (!N1N2MessageTransferReqData->n1n2_failure_txf_notif_uri) {
                    ogs_error("[%s:%d] No n1-n2-failure-notification-uri",
                            amf_ue->supi, sess->psi);
                    return OGS_ERROR;
                }

                rc = ogs_sbi_getaddr_from_uri(
                        &scheme, &fqdn, &fqdn_port, &addr, &addr6,
                        N1N2MessageTransferReqData->n1n2_failure_txf_notif_uri);
                if (rc == false || scheme == OpenAPI_uri_scheme_NULL) {
                    ogs_error("[%s:%d] Invalid URI [%s]",
                            amf_ue->supi, sess->psi,
                            N1N2MessageTransferReqData->
                                n1n2_failure_txf_notif_uri);
                    return OGS_ERROR;;
                }

                client = ogs_sbi_client_find(
                        scheme, fqdn, fqdn_port, addr, addr6);
                if (!client) {
                    ogs_debug("%s: ogs_sbi_client_add()", OGS_FUNC);
                    client = ogs_sbi_client_add(
                            scheme, fqdn, fqdn_port, addr, addr6);
                    if (!client) {
                        ogs_error("%s: ogs_sbi_client_add() failed", OGS_FUNC);

                        ogs_free(fqdn);
                        ogs_freeaddrinfo(addr);
                        ogs_freeaddrinfo(addr6);

                        return OGS_ERROR;
                    }
                }
                OGS_SBI_SETUP_CLIENT(&sess->paging, client);

                ogs_free(fqdn);
                ogs_freeaddrinfo(addr);
                ogs_freeaddrinfo(addr6);

                status = OGS_SBI_HTTP_STATUS_ACCEPTED;
                N1N2MessageTransferRspData.cause =
                    OpenAPI_n1_n2_message_transfer_cause_ATTEMPTING_TO_REACH_UE;

                /* Location */
                server = ogs_sbi_server_from_stream(stream);
                ogs_assert(server);

                memset(&header, 0, sizeof(header));
                header.service.name = (char *)OGS_SBI_SERVICE_NAME_NAMF_COMM;
                header.api.version = (char *)OGS_SBI_API_V1;
                header.resource.component[0] =
                    (char *)OGS_SBI_RESOURCE_NAME_UE_CONTEXTS;
                header.resource.component[1] = amf_ue->supi;
                header.resource.component[2] =
                    (char *)OGS_SBI_RESOURCE_NAME_N1_N2_MESSAGES;
                header.resource.component[3] = sess->sm_context_ref;

                sendmsg.http.location = ogs_sbi_server_uri(server, &header);

                /* Store Paging Info */
                AMF_SESS_STORE_PAGING_INFO(
                    sess, sendmsg.http.location,
                    N1N2MessageTransferReqData->n1n2_failure_txf_notif_uri);

                /* Store N2 Transfer message */
                AMF_SESS_STORE_N2_TRANSFER(
                        sess, pdu_session_resource_setup_request, n2buf);

                r = ngap_send_paging(amf_ue);
                ogs_expect(r == OGS_OK);
                ogs_assert(r != OGS_ERROR);

            } else if (CM_CONNECTED(amf_ue)) {
                r = nas_send_pdu_session_setup_request(sess, NULL, n2buf);
                ogs_expect(r == OGS_OK);
                ogs_assert(r != OGS_ERROR);

            } else {

                ogs_fatal("[%s] Invalid AMF-UE state", amf_ue->supi);
                ogs_assert_if_reached();

            }

        }
        break;

    case OpenAPI_ngap_ie_type_PDU_RES_MOD_REQ:
        if (!n1buf) {
            ogs_error("[%s] No N1 SM Content", amf_ue->supi);
            return OGS_ERROR;
        }
        if (!n2buf) {
            ogs_error("[%s] No N2 SM Content", amf_ue->supi);
            return OGS_ERROR;
        }

        if (CM_IDLE(amf_ue)) {
            ogs_sbi_server_t *server = NULL;
            ogs_sbi_header_t header;

            status = OGS_SBI_HTTP_STATUS_ACCEPTED;
            N1N2MessageTransferRspData.cause =
                OpenAPI_n1_n2_message_transfer_cause_ATTEMPTING_TO_REACH_UE;

            /* Location */
            server = ogs_sbi_server_from_stream(stream);
            ogs_assert(server);

            memset(&header, 0, sizeof(header));
            header.service.name = (char *)OGS_SBI_SERVICE_NAME_NAMF_COMM;
            header.api.version = (char *)OGS_SBI_API_V1;
            header.resource.component[0] =
                (char *)OGS_SBI_RESOURCE_NAME_UE_CONTEXTS;
            header.resource.component[1] = amf_ue->supi;
            header.resource.component[2] =
                (char *)OGS_SBI_RESOURCE_NAME_N1_N2_MESSAGES;
            header.resource.component[3] = sess->sm_context_ref;

            sendmsg.http.location = ogs_sbi_server_uri(server, &header);

            /* Store Paging Info */
            AMF_SESS_STORE_PAGING_INFO(
                    sess, sendmsg.http.location, NULL);

            /* Store 5GSM Message */
            AMF_SESS_STORE_5GSM_MESSAGE(sess,
                    OGS_NAS_5GS_PDU_SESSION_MODIFICATION_COMMAND,
                    n1buf, n2buf);

            r = ngap_send_paging(amf_ue);
            ogs_expect(r == OGS_OK);
            ogs_assert(r != OGS_ERROR);

        } else if (CM_CONNECTED(amf_ue)) {
            if (CONTEXT_SETUP_ESTABLISHED(amf_ue)) {
                r = nas_send_pdu_session_modification_command(
                        sess, n1buf, n2buf);
                ogs_expect(r == OGS_OK);
                ogs_assert(r != OGS_ERROR);
            } else {
                /* Store 5GSM Message */
                ogs_warn("[Session MODIFY] Context setup is not established");
                AMF_SESS_STORE_5GSM_MESSAGE(sess,
                        OGS_NAS_5GS_PDU_SESSION_MODIFICATION_COMMAND,
                        n1buf, n2buf);
            }
        } else {
            ogs_fatal("[%s] Invalid AMF-UE state", amf_ue->supi);
            ogs_assert_if_reached();
        }

        break;

    case OpenAPI_ngap_ie_type_PDU_RES_REL_CMD:
        if (!n2buf) {
            ogs_error("[%s] No N2 SM Content", amf_ue->supi);
            return OGS_ERROR;
        }

        if (CM_IDLE(amf_ue)) {
            if (N1N2MessageTransferReqData->is_skip_ind == true &&
                N1N2MessageTransferReqData->skip_ind == true) {

                if (n1buf)
                    ogs_pkbuf_free(n1buf);
                if (n2buf)
                    ogs_pkbuf_free(n2buf);

                N1N2MessageTransferRspData.cause =
                    OpenAPI_n1_n2_message_transfer_cause_N1_MSG_NOT_TRANSFERRED;

            } else {
                ogs_sbi_server_t *server = NULL;
                ogs_sbi_header_t header;

                status = OGS_SBI_HTTP_STATUS_ACCEPTED;
                N1N2MessageTransferRspData.cause =
                    OpenAPI_n1_n2_message_transfer_cause_ATTEMPTING_TO_REACH_UE;

                /* Location */
                server = ogs_sbi_server_from_stream(stream);
                ogs_assert(server);

                memset(&header, 0, sizeof(header));
                header.service.name = (char *)OGS_SBI_SERVICE_NAME_NAMF_COMM;
                header.api.version = (char *)OGS_SBI_API_V1;
                header.resource.component[0] =
                    (char *)OGS_SBI_RESOURCE_NAME_UE_CONTEXTS;
                header.resource.component[1] = amf_ue->supi;
                header.resource.component[2] =
                    (char *)OGS_SBI_RESOURCE_NAME_N1_N2_MESSAGES;
                header.resource.component[3] = sess->sm_context_ref;

                sendmsg.http.location = ogs_sbi_server_uri(server, &header);

                /* Store Paging Info */
                AMF_SESS_STORE_PAGING_INFO(
                        sess, sendmsg.http.location, NULL);

                /* Store 5GSM Message */
                AMF_SESS_STORE_5GSM_MESSAGE(sess,
                        OGS_NAS_5GS_PDU_SESSION_RELEASE_COMMAND,
                        n1buf, n2buf);

                r = ngap_send_paging(amf_ue);
                ogs_expect(r == OGS_OK);
                ogs_assert(r != OGS_ERROR);
            }

        } else if (CM_CONNECTED(amf_ue)) {
            if (CONTEXT_SETUP_ESTABLISHED(amf_ue)) {
                r = nas_send_pdu_session_release_command(sess, n1buf, n2buf);
                ogs_expect(r == OGS_OK);
                ogs_assert(r != OGS_ERROR);
            } else {
                /* Store 5GSM Message */
                ogs_warn("[Session RELEASE] Context setup is not established");
                AMF_SESS_STORE_5GSM_MESSAGE(sess,
                        OGS_NAS_5GS_PDU_SESSION_RELEASE_COMMAND,
                        n1buf, n2buf);
            }
        } else {
            ogs_fatal("[%s] Invalid AMF-UE state", amf_ue->supi);
            ogs_assert_if_reached();
        }
        break;

    case OpenAPI_ngap_ie_type_NULL:
        /*
         * No n2InfoContainer. According to TS23.502, this means that SMF has
         * encountered an error and is rejecting the session.
         *
         * TS23.502
         * 6.3.1.7 4.3.2.2 UE Requested PDU Session Establishment
         * p100
         * 11.  ...
         * If the PDU session establishment failed anywhere between step 5
         * and step 11, then the Namf_Communication_N1N2MessageTransfer
         * request shall include the N1 SM container with a PDU Session
         * Establishment Reject message ...
         */
        if (!n1buf) {
            ogs_error("[%s] No N1 SM Content", amf_ue->supi);
            return OGS_ERROR;
        }

        ogs_error("[%d:%d] PDU session establishment reject",
                sess->psi, sess->pti);

        r = nas_5gs_send_gsm_reject(sess,
                OGS_NAS_PAYLOAD_CONTAINER_N1_SM_INFORMATION, n1buf);
        ogs_expect(r == OGS_OK);
        ogs_assert(r != OGS_ERROR);

        amf_sess_remove(sess);
        break;

    default:
        ogs_error("Not implemented ngapIeType[%d]", ngapIeType);
        ogs_assert_if_reached();
    }

    response = ogs_sbi_build_response(&sendmsg, status);
    ogs_assert(response);
    ogs_assert(true == ogs_sbi_server_send_response(stream, response));

    if (sendmsg.http.location)
        ogs_free(sendmsg.http.location);

    return OGS_OK;
}


int amf_namf_callback_handle_sm_context_status(
        ogs_sbi_stream_t *stream, ogs_sbi_message_t *recvmsg)
{
    int status = OGS_SBI_HTTP_STATUS_NO_CONTENT;

    amf_ue_t *amf_ue = NULL;
    amf_sess_t *sess = NULL;
    
    uint8_t pdu_session_identity;

    ogs_sbi_message_t sendmsg;
    ogs_sbi_response_t *response = NULL;

    OpenAPI_sm_context_status_notification_t *SmContextStatusNotification;
    OpenAPI_status_info_t *StatusInfo;

    ogs_assert(stream);
    ogs_assert(recvmsg);

    if (!recvmsg->h.resource.component[0]) {
        status = OGS_SBI_HTTP_STATUS_BAD_REQUEST;
        ogs_error("No SUPI");
        goto cleanup;
    }

    amf_ue = amf_ue_find_by_supi(recvmsg->h.resource.component[0]);
    if (!amf_ue) {
        status = OGS_SBI_HTTP_STATUS_NOT_FOUND;
        ogs_error("Cannot find SUPI [%s]", recvmsg->h.resource.component[0]);
        goto cleanup;
    }

    if (!recvmsg->h.resource.component[2]) {
        status = OGS_SBI_HTTP_STATUS_BAD_REQUEST;
        ogs_error("[%s] No PDU Session Identity", amf_ue->supi);
        goto cleanup;
    }

    pdu_session_identity = atoi(recvmsg->h.resource.component[2]);
    if (pdu_session_identity == OGS_NAS_PDU_SESSION_IDENTITY_UNASSIGNED) {
        status = OGS_SBI_HTTP_STATUS_BAD_REQUEST;
        ogs_error("[%s] PDU Session Identity is unassigned", amf_ue->supi);
        goto cleanup;
    }

    sess = amf_sess_find_by_psi(amf_ue, pdu_session_identity);
    if (!sess) {
        status = OGS_SBI_HTTP_STATUS_NOT_FOUND;
        ogs_warn("[%s] Cannot find session", amf_ue->supi);
        goto cleanup;
    }

    SmContextStatusNotification = recvmsg->SmContextStatusNotification;
    if (!SmContextStatusNotification) {
        status = OGS_SBI_HTTP_STATUS_BAD_REQUEST;
        ogs_error("[%s:%d] No SmContextStatusNotification",
                amf_ue->supi, sess->psi);
        goto cleanup;
    }

    StatusInfo = SmContextStatusNotification->status_info;
    if (!StatusInfo) {
        status = OGS_SBI_HTTP_STATUS_BAD_REQUEST;
        ogs_error("[%s:%d] No StatusInfo", amf_ue->supi, sess->psi);
        goto cleanup;
    }

    sess->resource_status = StatusInfo->resource_status;

    /*
     * Race condition for PDU session release complete
     *  - CLIENT : /nsmf-pdusession/v1/sm-contexts/{smContextRef}/modify
     *  - SERVER : /namf-callback/v1/{supi}/sm-context-status/{psi})
     *
     * If NOTIFICATION is received before the CLIENT response is received,
     * CLIENT sync is not finished. In this case, the session context
     * should not be removed.
     *
     * If NOTIFICATION comes after the CLIENT response is received,
     * sync is done. So, the session context can be removed.
     */
    ogs_info("[%s:%d][%d:%d:%s] "
            "/namf-callback/v1/{supi}/sm-context-status/{psi}",
            amf_ue->supi, sess->psi,
            sess->n1_released, sess->n2_released,
            OpenAPI_resource_status_ToString(sess->resource_status));

    if (sess->n1_released == true &&
        sess->n2_released == true &&
        sess->resource_status == OpenAPI_resource_status_RELEASED) {
        amf_nsmf_pdusession_handle_release_sm_context(
                sess, AMF_RELEASE_SM_CONTEXT_NO_STATE);
    }

cleanup:
    memset(&sendmsg, 0, sizeof(sendmsg));

    response = ogs_sbi_build_response(&sendmsg, status);
    ogs_assert(response);
    ogs_assert(true == ogs_sbi_server_send_response(stream, response));

    return OGS_OK;
}

int amf_namf_callback_handle_dereg_notify(
        ogs_sbi_stream_t *stream, ogs_sbi_message_t *recvmsg)
{
    int r, state, status = OGS_SBI_HTTP_STATUS_NO_CONTENT;

    amf_ue_t *amf_ue = NULL;

    ogs_sbi_message_t sendmsg;
    ogs_sbi_response_t *response = NULL;

    OpenAPI_deregistration_data_t *DeregistrationData;

    ogs_assert(stream);
    ogs_assert(recvmsg);

    if (!recvmsg->h.resource.component[0]) {
        status = OGS_SBI_HTTP_STATUS_BAD_REQUEST;
        ogs_error("No SUPI");
        goto cleanup;
    }

    amf_ue = amf_ue_find_by_supi(recvmsg->h.resource.component[0]);
    if (!amf_ue) {
        status = OGS_SBI_HTTP_STATUS_NOT_FOUND;
        ogs_error("Cannot find SUPI [%s]", recvmsg->h.resource.component[0]);
        goto cleanup;
    }

    DeregistrationData = recvmsg->DeregistrationData;
    if (!DeregistrationData) {
        status = OGS_SBI_HTTP_STATUS_BAD_REQUEST;
        ogs_error("[%s] No DeregistrationData", amf_ue->supi);
        goto cleanup;
    }

    if (DeregistrationData->dereg_reason ==
            OpenAPI_deregistration_reason_NULL) {
        status = OGS_SBI_HTTP_STATUS_BAD_REQUEST;
        ogs_error("[%s] No Deregistraion Reason ", amf_ue->supi);
        goto cleanup;
    }

    if (DeregistrationData->access_type != OpenAPI_access_type_3GPP_ACCESS) {
        status = OGS_SBI_HTTP_STATUS_BAD_REQUEST;
        ogs_error("[%s] Deregistration access type not 3GPP", amf_ue->supi);
        goto cleanup;
    }

    ogs_info("Deregistration notify reason: %s:%s:%s",
        amf_ue->supi,
        OpenAPI_deregistration_reason_ToString(DeregistrationData->dereg_reason),
        OpenAPI_access_type_ToString(DeregistrationData->access_type));

    /*
     * TODO: do not start deregistration if UE has emergency sessions
     * 4.2.2.3.3
     * If the UE has established PDU Session associated with emergency service, the AMF shall not initiate
     * Deregistration procedure. In this case, the AMF performs network requested PDU Session Release for any PDU
     * session associated with non-emergency service as described in clause 4.3.4.
     */

    /*
     * - AMF_NETWORK_INITIATED_EXPLICIT_DE_REGISTERED
     * 1. UDM_UECM_DeregistrationNotification
     * 2. Deregistration request
     * 3. UDM_SDM_Unsubscribe
     * 4. UDM_UECM_Deregisration
     * 5. PDU session release request
     * 6. PDUSessionResourceReleaseCommand +
     *    PDU session release command
     * 7. PDUSessionResourceReleaseResponse
     * 8. AM_Policy_Association_Termination
     * 9.  Deregistration accept
     * 10. Signalling Connecion Release
     */
    if (CM_CONNECTED(amf_ue)) {
        r = nas_5gs_send_de_registration_request(
                amf_ue,
                DeregistrationData->dereg_reason,
                OGS_5GMM_CAUSE_5GS_SERVICES_NOT_ALLOWED);
        ogs_expect(r == OGS_OK);
        ogs_assert(r != OGS_ERROR);

        state = AMF_NETWORK_INITIATED_EXPLICIT_DE_REGISTERED;

    } else if (CM_IDLE(amf_ue)) {
        ogs_error("Not implemented : Use Implicit De-registration");

        state = AMF_NETWORK_INITIATED_IMPLICIT_DE_REGISTERED;

    } else {
        ogs_fatal("Invalid State");
        ogs_assert_if_reached();
    }

    if (UDM_SDM_SUBSCRIBED(amf_ue)) {
        r = amf_ue_sbi_discover_and_send(
                OGS_SBI_SERVICE_TYPE_NUDM_SDM, NULL,
                amf_nudm_sdm_build_subscription_delete,
                amf_ue, state, NULL);
        ogs_expect(r == OGS_OK);
        ogs_assert(r != OGS_ERROR);
    } else if (PCF_AM_POLICY_ASSOCIATED(amf_ue)) {
        r = amf_ue_sbi_discover_and_send(
                OGS_SBI_SERVICE_TYPE_NPCF_AM_POLICY_CONTROL,
                NULL,
                amf_npcf_am_policy_control_build_delete,
                amf_ue, state, NULL);
        ogs_expect(r == OGS_OK);
        ogs_assert(r != OGS_ERROR);
    }

cleanup:
    memset(&sendmsg, 0, sizeof(sendmsg));

    response = ogs_sbi_build_response(&sendmsg, status);
    ogs_assert(response);
    ogs_assert(true == ogs_sbi_server_send_response(stream, response));

    return OGS_OK;
}

static int update_rat_res_add_one(cJSON *restriction,
                                  OpenAPI_list_t *restrictions, long index)
{
    void *restr;

    if (!cJSON_IsString(restriction)) {
        ogs_error("Invalid type of ratRestriction element");
        return OGS_ERROR;
    }

    restr = (void *) OpenAPI_rat_type_FromString(cJSON_GetStringValue(restriction));
    if (!restr) {
        ogs_error("No restr");
        return OGS_ERROR;
    }

    if (index == restrictions->count) {
        OpenAPI_list_add(restrictions, restr);
    } else if (restrictions->count < index && index <= 0) {
        OpenAPI_list_insert_prev(
            restrictions, OpenAPI_list_find(restrictions, index), restr);
    } else {
        ogs_error("Can't add RAT restriction to invalid index");
        return OGS_ERROR;
    }
    return OGS_OK;
}

static int update_rat_res_array(cJSON *json_restrictions,
                                OpenAPI_list_t *restrictions)
{
    cJSON *restriction;

    if (!cJSON_IsArray(json_restrictions)) {
        ogs_error("Invalid type of ratRestrictions");
        return OGS_ERROR;
    }

    OpenAPI_list_clear(restrictions);

    cJSON_ArrayForEach(restriction, json_restrictions) {
        if (update_rat_res_add_one(restriction, restrictions,
                                   restrictions->count) != OGS_OK) {
            return OGS_ERROR;
        }
    }
    return OGS_OK;
}

static int update_rat_res(OpenAPI_change_item_t *item_change,
                          OpenAPI_list_t *restrictions)
{
    cJSON* json = item_change->new_value->json;
    cJSON* json_restrictions;

    if (!item_change->path) {
        return OGS_ERROR;
    }

    switch (item_change->op) {
    case OpenAPI_change_type_REPLACE:
    case OpenAPI_change_type_ADD:
        if (!strcmp(item_change->path, "")) {
            if (!cJSON_IsObject(json)) {
                ogs_error("Invalid type of am-data");
            }
            json_restrictions = cJSON_GetObjectItemCaseSensitive(
                                    json, "ratRestrictions");
            if (json_restrictions) {
                return update_rat_res_array(json_restrictions, restrictions);
            } else {
                return OGS_OK;
            }
        } else if (!strcmp(item_change->path, "/ratRestrictions")) {
            return update_rat_res_array(json, restrictions);
        } else if (strstr(item_change->path, "/ratRestrictions/") ==
                   item_change->path) {
            char *index = item_change->path + strlen("/ratRestrictions/");
            long i = strcmp(index, "-") ? atol(index) : restrictions->count;

            return update_rat_res_add_one(json, restrictions, i);
        }
        return OGS_OK;

    case OpenAPI_change_type__REMOVE:
        if (!strcmp(item_change->path, "")) {
            OpenAPI_list_clear(restrictions);
            return OGS_OK;
        } else if (!strcmp(item_change->path, "/ratRestrictions")) {
            OpenAPI_list_clear(restrictions);
            return OGS_OK;
        } else if (strstr(item_change->path, "/ratRestrictions/") ==
                   item_change->path) {
            char *index = item_change->path + strlen("/ratRestrictions/");
            long i = atol(index);

            if (restrictions->count < i && i <= 0) {
            OpenAPI_list_remove(
                restrictions, OpenAPI_list_find(restrictions, i));
            } else {
                ogs_error("Can't add RAT restriction to invalid index");
                return OGS_ERROR;
            }
        }
        return OGS_OK;

    default:
        return OGS_OK;
    }

}

static int update_ambr_check_one(cJSON *obj, uint64_t *limit,
                                 bool *ambr_changed)
{
    if (!cJSON_IsString(obj)) {
        ogs_error("Invalid type of subscribedUeAmbr");
        return OGS_ERROR;
    }
    *limit = ogs_sbi_bitrate_from_string(obj->valuestring);
    *ambr_changed = true;
    return OGS_OK;
}

static int update_ambr_check_obj(cJSON *obj, ogs_bitrate_t *ambr,
                                 bool *ambr_changed)
{
    if (!cJSON_IsObject(obj)) {
        if (obj == NULL || cJSON_IsNull(obj)) {
            /* Limit of 0 means unlimited. */
            ambr->uplink = 0;
            ambr->downlink = 0;
            *ambr_changed = true;
            return OGS_OK;
        } else {
            ogs_error("Invalid type of subscribedUeAmbr");
            return OGS_ERROR;
        }
    }

    if (update_ambr_check_one(
            cJSON_GetObjectItemCaseSensitive(obj, "uplink"),
            &ambr->uplink, ambr_changed)) {
        return OGS_ERROR;
    }
    if (update_ambr_check_one(
            cJSON_GetObjectItemCaseSensitive(obj, "downlink"),
            &ambr->downlink, ambr_changed)) {
        return OGS_ERROR;
    }
    return OGS_OK;
}

static int update_ambr(OpenAPI_change_item_t *item_change,
                       ogs_bitrate_t *ambr, bool *ambr_changed)
{
    cJSON* json = item_change->new_value->json;

    if (!item_change->path) {
        return OGS_ERROR;
    }

    switch (item_change->op) {
    case OpenAPI_change_type_REPLACE:
    case OpenAPI_change_type_ADD:
        if (!strcmp(item_change->path, "")) {
            if (!cJSON_IsObject(json)) {
                ogs_error("Invalid type of am-data");
            }
            return update_ambr_check_obj(
                cJSON_GetObjectItemCaseSensitive(json, "subscribedUeAmbr"),
                ambr, ambr_changed);
        } else if (!strcmp(item_change->path, "/subscribedUeAmbr")) {
            return update_ambr_check_obj(json, ambr, ambr_changed);
        } else if (!strcmp(item_change->path, "/subscribedUeAmbr/uplink")) {
            return update_ambr_check_one(json, &ambr->uplink, ambr_changed);
        } else if (!strcmp(item_change->path, "/subscribedUeAmbr/downlink")) {
            return update_ambr_check_one(json, &ambr->downlink, ambr_changed);
        }
        return OGS_OK;


    case OpenAPI_change_type__REMOVE:
        if (!strcmp(item_change->path, "/subscribedUeAmbr")) {
            update_ambr_check_obj(NULL, ambr, ambr_changed);
        }
        return OGS_OK;
    default:
        return OGS_OK;
    }
}

int amf_namf_callback_handle_sdm_data_change_notify(
        ogs_sbi_stream_t *stream, ogs_sbi_message_t *recvmsg)
{
    int r, state, status = OGS_SBI_HTTP_STATUS_NO_CONTENT;

    amf_ue_t *amf_ue = NULL;

    ogs_sbi_message_t sendmsg;
    ogs_sbi_response_t *response = NULL;

    OpenAPI_modification_notification_t *ModificationNotification;
    OpenAPI_lnode_t *node;

    char *ueid = NULL;
    char *res_name = NULL;

    bool ambr_changed = false;

    ogs_assert(stream);
    ogs_assert(recvmsg);

    ModificationNotification = recvmsg->ModificationNotification;
    if (!ModificationNotification) {
        status = OGS_SBI_HTTP_STATUS_BAD_REQUEST;
        ogs_error("No ModificationNotification");
        goto cleanup;
    }

    OpenAPI_list_for_each(ModificationNotification->notify_items, node) {
        OpenAPI_notify_item_t *item = node->data;

        char *saveptr = NULL;

        ueid = ogs_sbi_parse_uri(item->resource_id, "/", &saveptr);
        if (!ueid) {
            status = OGS_SBI_HTTP_STATUS_BAD_REQUEST;
            ogs_error("[%s] No UeId", item->resource_id);
            goto cleanup;
        }

        amf_ue = amf_ue_find_by_supi(ueid);
        if (!amf_ue) {
            status = OGS_SBI_HTTP_STATUS_NOT_FOUND;
            ogs_error("Cannot find SUPI [%s]", ueid);
            goto cleanup;
        }

        res_name = ogs_sbi_parse_uri(NULL, "/", &saveptr);
        if (!res_name) {
            status = OGS_SBI_HTTP_STATUS_BAD_REQUEST;
            ogs_error("[%s] No Resource Name", item->resource_id);
            goto cleanup;
        }

        SWITCH(res_name)
        CASE(OGS_SBI_RESOURCE_NAME_AM_DATA)
            OpenAPI_lnode_t *node_ci;

            OpenAPI_list_for_each(item->changes, node_ci) {
                OpenAPI_change_item_t *change_item = node_ci->data;
                if (update_rat_res(change_item, amf_ue->rat_restrictions) ||
                        update_ambr(change_item, &amf_ue->ue_ambr,
                            &ambr_changed)) {
                    status = OGS_SBI_HTTP_STATUS_BAD_REQUEST;
                    goto cleanup;
                }
            }
            break;
        DEFAULT
            status = OGS_SBI_HTTP_STATUS_BAD_REQUEST;
            ogs_error("Unknown Resource Name: [%s]", res_name);
            goto cleanup;
        END

        ogs_free(ueid);
        ogs_free(res_name);

        ueid = NULL;
        res_name = NULL;
    }

    if (amf_ue) {
        ran_ue_t *ran_ue = ran_ue_cycle(amf_ue->ran_ue);
        if ((!ran_ue) || (amf_ue_is_rat_restricted(amf_ue))) {

            if (!ran_ue) {
                ogs_error("NG context has already been removed");
                /* ran_ue is required for amf_ue_is_rat_restricted() */

                ogs_error("Not implemented : Use Implicit De-registration");
                state = AMF_NETWORK_INITIATED_IMPLICIT_DE_REGISTERED;
            }
            else {
                /*
                * - AMF_NETWORK_INITIATED_EXPLICIT_DE_REGISTERED
                * 1. UDM_UECM_DeregistrationNotification
                * 2. Deregistration request
                * 3. UDM_SDM_Unsubscribe
                * 4. UDM_UECM_Deregistration
                * 5. PDU session release request
                * 6. PDUSessionResourceReleaseCommand +
                *    PDU session release command
                * 7. PDUSessionResourceReleaseResponse
                * 8. AM_Policy_Association_Termination
                * 9.  Deregistration accept
                * 10. Signalling Connection Release
                */
                r = nas_5gs_send_de_registration_request(
                        amf_ue,
                        OpenAPI_deregistration_reason_REREGISTRATION_REQUIRED, 0);
                ogs_expect(r == OGS_OK);
                ogs_assert(r != OGS_ERROR);

                state = AMF_NETWORK_INITIATED_EXPLICIT_DE_REGISTERED;
            }

            if (UDM_SDM_SUBSCRIBED(amf_ue)) {
                r = amf_ue_sbi_discover_and_send(
                        OGS_SBI_SERVICE_TYPE_NUDM_SDM, NULL,
                        amf_nudm_sdm_build_subscription_delete,
                        amf_ue, state, NULL);
                ogs_expect(r == OGS_OK);
                ogs_assert(r != OGS_ERROR);
            } else if (PCF_AM_POLICY_ASSOCIATED(amf_ue)) {
                r = amf_ue_sbi_discover_and_send(
                        OGS_SBI_SERVICE_TYPE_NPCF_AM_POLICY_CONTROL,
                        NULL,
                        amf_npcf_am_policy_control_build_delete,
                        amf_ue, state, NULL);
                ogs_expect(r == OGS_OK);
                ogs_assert(r != OGS_ERROR);
            }

        } else if (ambr_changed) {
            ogs_pkbuf_t *ngapbuf;

            ngapbuf = ngap_build_ue_context_modification_request(amf_ue);
            ogs_assert(ngapbuf);

            r = nas_5gs_send_to_gnb(amf_ue, ngapbuf);
            ogs_expect(r == OGS_OK);
            ogs_assert(r != OGS_ERROR);
        }
    }

cleanup:
    if (ueid)
        ogs_free(ueid);
    if (res_name)
        ogs_free(res_name);

    memset(&sendmsg, 0, sizeof(sendmsg));

    response = ogs_sbi_build_response(&sendmsg, status);
    ogs_assert(response);
    ogs_assert(true == ogs_sbi_server_send_response(stream, response));

    return OGS_OK;
}
